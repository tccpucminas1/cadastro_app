package com.cadastroUsuario.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import com.cadastroUsuario.models.Usuario;

@Service
public class CadastroProducer {

    @Autowired
    KafkaTemplate<String, String> kafkaservice;

    public void enviarNovoUsuario(Usuario usuario)
    {
        if (usuario.getTipo().equals("associado")){

            kafkaservice.send("boasaude.topico.cadastro.associado", "prestador" + usuario.getCpf());
        }
        else
        {
            kafkaservice.send("boasaude.topico.cadastro.prestador", "prestador" + usuario.getCpf());
        }
    }


}