package com.cadastroUsuario.services;


import com.cadastroUsuario.CadastroApplication;
import com.cadastroUsuario.models.DTOs.UsuarioEntradaDTO;
import com.cadastroUsuario.models.Usuario;
import com.cadastroUsuario.repositories.UsuarioRepository;
import javassist.bytecode.stackmap.BasicBlock;
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;
import org.aspectj.bridge.IMessage;
import org.aspectj.bridge.IMessageContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.Optional;
import com.cadastroUsuario.producer.CadastroProducer;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private CadastroProducer cadastroProducer;

    public Usuario cadastrarUsuario(Usuario usuario)
    {
        Usuario objetoUsuario = usuarioRepository.save(usuario);

        cadastroProducer.enviarNovoUsuario(objetoUsuario);

        return objetoUsuario;
    }

    public Usuario alterarUsuario(int id, UsuarioEntradaDTO usuario)
    {
        try {
            Usuario usuarioCadastrado = consultarUsuarioPorId(id);
            usuarioCadastrado.setNome(usuario.getCpf());
            usuarioCadastrado.setSobrenome(usuario.getSobrenome());
            usuarioCadastrado.setCpf(usuario.getCpf());
            usuarioCadastrado.setRg(usuario.getRg());
            usuarioCadastrado.setEmail(usuario.getEmail());
            usuarioCadastrado.setCelular(usuario.getCelular());
            usuarioCadastrado.setGenero(usuario.getGenero());
            usuarioCadastrado.setDataNascimento(usuario.getDatanascimento());
            usuarioCadastrado.setEndereco(usuario.getEndereco());
            usuarioCadastrado.setLocalizacao(usuario.getLocalizacao());
            usuarioCadastrado.setTipo(usuario.getTipo());
            usuarioCadastrado.setProfissao(usuario.getProfissao());
            usuarioCadastrado.setSenha(usuario.getSenha());

            Usuario objetoUsuario = usuarioRepository.save(usuarioCadastrado);
            return objetoUsuario;
        }
        catch (RuntimeException exception) {
            throw new RuntimeException(exception.getMessage());
        }
    }

    public Usuario consultarUsuarioPorId(int idUsuario)
    {
        Optional<Usuario> optUsuario;
        optUsuario = usuarioRepository.findById(idUsuario);

        if (optUsuario.isPresent())
        {
            return optUsuario.get();
        }
        else
        {
            throw  new RuntimeException("Usuário não localizado no sistema");
        }
    }

    public Usuario consultarUsuarioPorEmail(String email)
    {
        Optional<Usuario> optUsuario;
        optUsuario = usuarioRepository.findByEmail(email);

        if (optUsuario.isPresent())
        {
            return optUsuario.get();
        }
        else
        {
            throw  new RuntimeException("Usuário não localizado no sistema!");
        }
    }

    public Usuario validarUsuario(String email, String senha)
    {
        Optional<Usuario> optUsuario;
        optUsuario = usuarioRepository.findByEmailAndSenha(email, senha);

        if (optUsuario.isPresent())
        {
            return optUsuario.get();
        }
        else
        {
            throw  new RuntimeException("Usuário inválido!");
        }
    }




    public Iterable<Usuario> ListarUsuarios()
    {
        Iterable<Usuario> listaUsuarios = usuarioRepository.findAll();
        return listaUsuarios;
    }
}
