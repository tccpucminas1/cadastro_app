package com.cadastroUsuario.models;

import com.sun.istack.NotNull;
import org.springframework.cglib.core.internal.LoadingCache;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String nome;

    @NotNull
    private String sobrenome;

    @NotNull
    private String rg;

    @NotNull
    private String cpf;

    @NotNull
    private LocalDate datanascimento;

    @NotNull
    private String genero;

    @NotNull
    private String endereco;

    @NotNull
    private String celular;

    @NotNull
    private String email;

    @NotNull
    private String senha;

    @NotNull
    private String localizacao;

    @NotNull
    private LocalDate datacadastro;

    @NotNull
    private String tipo;

    private String profissao;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {this.rg = rg;}

    public LocalDate getDataNascimento() {return datanascimento;}

    public void setDataNascimento(LocalDate datanascimento) {this.datanascimento = datanascimento;}

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {this.genero = genero;}

    public String getEndereco() {return endereco;}

    public void setEndereco(String endereco ) {this.endereco = endereco;}

    public String getCelular() {return celular;}

    public void setCelular(String celular) {this.celular = celular;}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getLocalizacao() { return localizacao; }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public LocalDate getDatacadastro() {return datacadastro;}

    public void setDatacadastro(LocalDate datacadastro) {this.datacadastro = datacadastro;}

    public String getTipo() { return tipo; }

    public void setTipo(String tipo) {this.tipo = tipo;}

    public String getProfissao() { return profissao; }

    public void setProfissao(String profissao) {this.profissao = tipo;}

    public Usuario() {

    }

}


