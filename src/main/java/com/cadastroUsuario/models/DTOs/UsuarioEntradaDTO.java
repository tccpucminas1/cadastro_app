package com.cadastroUsuario.models.DTOs;

import org.springframework.cglib.core.internal.LoadingCache;

import java.time.LocalDate;

public class UsuarioEntradaDTO
{

    private String nome;
    private String sobrenome;
    private String rg;
    private String cpf;
    private LocalDate datanascimento;
    private String genero;
    private String endereco;
    private String celular;
    private String email;
    private String senha;
    private String localizacao;
    private String tipo;
    private String profissao;

    public String getNomecompleto() {
        return nome;
    }
    public void setNomecompleto(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }
    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }
    public void setRg(String rg) {
        this.rg = rg;
    }

    public LocalDate getDatanascimento() {
        return datanascimento;
    }
    public void setDataNascimento(LocalDate datanascimento) { this.datanascimento = datanascimento;}

    public String getGenero() {
        return genero;
    }
    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCelular() {
        return celular;
    }
    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getLocalizacao() {
        return localizacao;
    }
    public void setLocalizacao(String localizacao) {this.localizacao = localizacao;}

    public String getTipo() {
        return tipo;
    }
    public void setTipo(String localizacao) {this.tipo = tipo;}

    public String getProfissao() {
        return profissao;
    }
    public void setProfissao(String profissao) {this.tipo = profissao;}

    public UsuarioEntradaDTO() {

    }
}

