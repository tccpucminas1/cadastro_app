## Usuario

### GET /usuario
Exibe a lista de usuários cadastrados no sistema.

**Response 200**
```json
[
  {
    "id": 1,
    "nome": "Exemplo1",
    "sobrenome": "Sobrenome Exemplo1",
    "rg": "30030030009",
    "cpf": "30030030009",
    "genero": "feminino",
    "endereco": "Rua do Exemplo2, 123, São Paulo, SP",
    "celular": "11995454545",
    "email": "exemplo1@gmail.com",
    "senha": "*********",
    "localizacao": "123321",
    "datacadastro": "2022-03-01",
    "tipo": "conveniado",
    "profissao": "médico" ,
    "dataNascimento": "1996-07-04"
  },
  {
    "id": 2,
    "nome": "Exemplo2",
    "sobrenome": "Sobrenome Exemplo2",
    "rg": "30030030009",
    "cpf": "30030030009",
    "genero": "feminino",
    "endereco": "Rua do Exemplo2, 123, São Paulo, SP",
    "celular": "11995454545",
    "email": "exemplo2@gmail.com",
    "senha": "*********",
    "localizacao": "123321",
    "datacadastro": "2022-03-04",
    "tipo": "conveniado",
    "profissao":"não informado",
    "dataNascimento": "1996-07-04"
  }
]
```
### POST /usuario
Cadastra um novo usuario no sistema.

**Request Body**
```json
{
  "nome" : "Exemplo1",
  "sobrenome" : "Sobrenome Exemplo1",
  "rg" : "30030030009",
  "cpf" : "30030030009",
  "dataNascimento" : "1996-07-04",
  "genero": "feminino",
  "endereco": "Rua do Exemplo1, 123, São Paulo, SP",
  "celular": 11995454545,
  "localizacao" : "123321",
  "tipo" : "conveniado",
  "profissão" : "não informado",
  "email" : "exemplo1@gmail.com",
  "senha" : "*********"
}
```

**Response 201**
```json
{
  "nome" : "Exemplo1",
  "sobrenome" : "Sobrenome Exemplo1",
  "rg" : "30030030009",
  "cpf" : "30030030009",
  "dataNascimento" : "1996-07-04",
  "genero": "feminino",
  "endereco": "Rua do Exemplo1, 123, São Paulo, SP",
  "celular": 11995454545,
  "localizacao" : "123321",
  "tipo" : "conveniado",
  "profissão" : "não informado",
  "email" : "exemplo1@gmail.com",
  "senha" : "*********"
}
```

### PUT /usuario/{id}
Atualiza os dados de um usuario.

**Request Body**
```json
{
  "nome" : "Exemplo1",
  "sobrenome" : "Sobrenome Exemplo1",
  "rg" : "30030030009",
  "cpf" : "30030030009",
  "dataNascimento" : "1996-07-04",
  "genero": "feminino",
  "endereco": "Rua do Exemplo1, 123, São Paulo, SP",
  "celular": 11995454545,
  "localizacao" : "123321",
  "tipo" : "conveniado",
  "profissão" : "não informado",
  "email" : "exemplo1@gmail.com",
  "senha" : "*********"
}
```
**Response 200**
```json
{
  "id" : 1,
  "nome" : "Exemplo1",
  "sobrenome" : "Sobrenome Exemplo1",
  "rg" : "30030030009",
  "cpf" : "30030030009",
  "dataNascimento" : "1996-07-04",
  "genero": "feminino",
  "endereco": "Rua do Exemplo1, 123, São Paulo, SP",
  "celular": 11995454545,
  "localizacao" : "123321",
  "tipo" : "conveniado",
  "profissão" : "não informado",
  "email" : "exemplo1@gmail.com",
  "senha" : "*********"
}
```

### GET /usuario/emai/{email]
Consular usuário por e-mail.
**Response 200**
```json
{
  "id" : 1,
  "nome" : "Exemplo1",
  "sobrenome" : "Sobrenome Exemplo1",
  "rg" : "30030030009",
  "cpf" : "30030030009",
  "dataNascimento" : "1996-07-04",
  "genero": "feminino",
  "endereco": "Rua do Exemplo1, 123, São Paulo, SP",
  "celular": 11995454545,
  "localizacao" : "123321",
  "tipo" : "conveniado",
  "profissão" : "não informado",
  "email" : "exemplo1@gmail.com",
  "senha" : "*********"
}
```
**Response 404**
Cadastro não localizado

### GET /usuario/acessovalido
Consular usuário por e-mail e senha.
**Response 200**
```json
{
  
  "email" : "exemplo1@gmail.com",
  "senha" : "*********"
}
```
**Response 404**
Cadastro não localizado